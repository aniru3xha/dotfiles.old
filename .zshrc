# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=5000

GOPATH="$HOME/.local/share/nvim/site/pack/packer/opt/go"

setopt HIST_IGNORE_ALL_DUPS
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/aniru3xha/.zshrc'

path+=('/home/aniru3xha/.local/scripts')
path+=('/home/aniru3xha/.cargo/bin')

autoload -Uz compinit
compinit
# End of lines added by compinstall

eval "$(starship init zsh)"
# xrandr --dpi 96

# Plugin
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Bindings
bindkey '^P' history-substring-search-up
bindkey '^N' history-substring-search-down

# Aliases
alias ll="ls -al"
alias r="ranger"

alias lazydocker="docker exec -it lazydocker lazydocker"

alias cddocker="cd ~/.local/containers"
alias cdgate="cd ~/Documents/GATE"
alias cdnvim="cd ~/.config/nvim"
alias cdrepos="cd ~/.local/repos"
alias cdscripts="cd ~/.local/scripts"
alias cdwebt="cd ~/Projects/tanishq.atulkar.xyz"
alias cdweba="cd ~/Projects/aniruddha.atulkar.xyz"

ZSH_AUTOSUGGEST_STRATEGY=completion
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20

export VISUAL=nvim
export EDITOR=nvim
export PATH
