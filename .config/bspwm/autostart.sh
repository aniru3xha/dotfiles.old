#!/bin/sh


pulseaudio -k &

dunst &
wal -stq -i ~/Pictures/ --saturate 0.4
light -S 10
light -Srs sysfs/leds/asus::kbd_backlight 3
picom &
$HOME/.config/polybar/launch.sh &
redshift -l 22.10317:78.17259 &
udiskie &

notify-send -i '/usr/share/icons/Adwaita/16x16/status/avatar-default.png' "Welcome $USER!"
