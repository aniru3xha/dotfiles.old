#!/usr/bin/env bash

kill $(pidof polybar)

# Enumerate through every monitors
for m in $(polybar --list-monitors | cut -d":" -f1); do
    MONITOR=$m polybar --reload workspace &

    MONITOR=$m polybar --reload net &
    MONITOR=$m polybar --reload temp &
    MONITOR=$m polybar --reload packs &
    MONITOR=$m polybar --reload audio &
    MONITOR=$m polybar --reload fs &
    MONITOR=$m polybar --reload bat &
    MONITOR=$m polybar --reload cal &
    MONITOR=$m polybar --reload void &
done
